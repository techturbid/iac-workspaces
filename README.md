## iac-workspaces

#### This repository will be used to store my Terraform Cloud Workspaces

This is necessary to be compliant with Terraform Cloud. Check this [link](https://www.terraform.io/docs/cloud/registry/publish.html#preparing-a-module-repository) for more information

I will add more information about the use of Terraform Cloud in the near future


